﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeInteractibleAfterDelay : MonoBehaviour
{

    public List<Button> toActivate;
    public float delay = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Activate", delay);
    }

    // Update is called once per frame
    void Activate()
    {
        foreach (var item in toActivate) {

        item.interactable = true;
        }
    }
}
