﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnObjectAfterDelay : MonoBehaviour {

    public float delay = 0.5f;
    public GameObject objectgToTuyrnOn;

	// Use this for initialization
	void Start () {
        Invoke("TurnOn", delay);
	}
	
	// Update is called once per frame
	void TurnOn () {
        objectgToTuyrnOn.SetActive(true);

    }
}
