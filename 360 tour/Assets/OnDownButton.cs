﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;


public class OnDownButton : MonoBehaviour, IPointerDownHandler
{

	public UnityEvent OnDown;

    public void OnPointerDown(PointerEventData eventData) {
        OnDown.Invoke();

        Debug.Log(this.gameObject.name + " Was Clicked.");
    }
}
