﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class Controller : MonoBehaviour
{

    public static Controller s;
    public string brakeTest, pong, tour, tour2;
    public int roomToLoad = 0;
    public bool firstLoad = true;
    public GameObject controlsCanvas;
    public bool showCaptions = true;
    public VideoControls activeControls;

    //[Header("Closed Captions")]
    //public List<ClosedCaption> sequence01Captions;
    //public List<ClosedCaption> sequence02Captions, sequence03Captions, sequence04Captions,
    //sequence05Captions, sequence06Captions, sequence07Captions, sequence08Captions, sequence09Captions,
    //sequence10Captions, sequence11Captions, sequence12Captions, sequence13Captions, sequence14Captions;

    public Text captionText, roomName;
    public string[] roomNames;

    public void UpdateRoomName(int i) {
        if (roomName && roomNames.Length > i) {
            roomName.text = roomNames[i];
        }
    }

    public void ReloadScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void Awake() {
        if (s == null) {
            s = this;
        } else {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        SceneManager.sceneLoaded += SceneLoad;
    }

    bool loadScene = false;

    public void LoadTour(int room, bool loadScene = false) {
        //teleport main camera object to the sphere with this index. needs a script and collection on the camera obj
        firstLoad = false;
        if (loadScene) {
            this.loadScene = true;
        }

        StartCoroutine("LoadTourOverTime");
        //SceneManager.LoadScene(tour);

        //handle teleport in onsceneload
        roomToLoad = room;
    }

    IEnumerator LoadTourOverTime() {
        //SceneManager.LoadScene("SampleScene");
        //yield return new WaitForSeconds(0.25f);
        if (this.loadScene) {
            string s;
            //if (SceneManager.GetActiveScene().name == tour) {
            //    s = tour2;
            //} else {
            s = tour;
            //}
            
            SceneManager.LoadScene(s);
            yield return new WaitForSeconds(0.1f);
            loadScene = false;
        }

        //foreach (var v in FindObjectsOfType<VideoPlayer>()) {
        //    //print(v.name + " is being prepped.");

        //    //v.Prepare();
        //    //WaitForSeconds delay = new WaitForSeconds(0.25f);
        //    //    yield return delay;


        //    //print(v.name + " is prepped.");


        //    if (v.gameObject.tag == "TV") {
        //        WaitForSeconds delay = new WaitForSeconds(0.1f);

        //        v.GetComponent<Interactive>().PrepVideo();

        //        while (!v.isPrepared) {
        //            yield return delay;
        //            break;
        //        }
        //        v.Play();
        //    }

        //}

        captionText = GameObject.Find("CaptionsText").GetComponent<Text>();

        yield return null;

    }

    void SceneLoad(Scene scene, LoadSceneMode mode) {
        //print("scene load");

        if (s != this) {
            return;
        }

        if (scene.name == tour || scene.name == tour2) {
            //print("scene load is tour");

            //loaded tour scene, teleport to proper room
            FindObjectOfType<CameraController>().teleportTo(roomToLoad);

            if (!firstLoad) {
                Camera.main.transform.GetChild(0).gameObject.SetActive(false);
                FindObjectOfType<CameraController>().joystick.SetActive(true);
            }

            if (!roomName) {
                if (GameObject.Find("RoomNameText")) {
                    roomName = GameObject.Find("RoomNameText").GetComponent<Text>();
                }
            }

            Controller.s.UpdateRoomName(roomToLoad);

        }

    }

    public void ReloadSceneAndMoveRooms(int destinationRoomNumber) {
        Controller.s.firstLoad = false;
        Controller.s.roomToLoad = destinationRoomNumber;
        Controller.s.ReloadScene();
    }

    public void ReloadThenPlayVideo(int roomToLoad) {
        ReloadSceneAndMoveRooms(roomToLoad);

        Invoke("TestLoad", 1.0f);

    }

    void TestLoad() {

        FindObjectOfType<CameraController>().rooms[roomToLoad].GetComponentInChildren<Interactive>().Interact();
    }

    public void LoadBrakeTest() {
        SceneManager.LoadScene(brakeTest);
    }

    public void LoadPong() {
        SceneManager.LoadScene(pong);
    }

}

[System.Serializable]
public struct ClosedCaption
{
    public string caption;
    public float timestamp;
}