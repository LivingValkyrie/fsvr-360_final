﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStopper : MonoBehaviour {

    public int roomToReturnTo = 2;

    public void ReturnToTour() {
        Controller.s.LoadTour(roomToReturnTo, true);
    }

    public void Restart() {
        Controller.s.ReloadScene();
    }
}
