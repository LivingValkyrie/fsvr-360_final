﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOutro : MonoBehaviour
{
    public List<GameObject> toEnable, objectsToDisable;
    void OnMouseDown() {
        foreach (var item in toEnable) {
            item.SetActive(true);
        }

        foreach (var g in objectsToDisable) {
            g.SetActive(false);
        }
    }

    //public override void Interact() {
    //    foreach (var item in toEnable) {
    //        item.SetActive(true);
    //    }

    //    foreach (var g in objectsToDisable) {
    //        g.SetActive(false);
    //    }
    //}
}