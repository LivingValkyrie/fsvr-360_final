﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoControls : MonoBehaviour {

    public GameObject PauseButton;
    public GameObject PlayButton;
    public GameObject StopButton;
    public int roomNumber;

    public void EnableVidControls() {
        PauseButton.SetActive(true);
        PlayButton.SetActive(false);
        StopButton.SetActive(true);
    }

    public void PauseVideo() {
        GetComponentInParent<VideoPlayer>().Pause();
        PauseButton.SetActive(false);
        PlayButton.SetActive(true);
        StopButton.SetActive(true);
    }

    public void PlayVideo() {
        GetComponentInParent<VideoPlayer>().Play();
        GetComponentInParent<Interactive>().StartSubs();

        PlayButton.SetActive(false);
        PauseButton.SetActive(true);
        StopButton.SetActive(true);
    }

    public void StopVideo() {
        //print("was called");
        GetComponentInParent<VideoPlayer>().Stop();
        GetComponentInParent<Interactive>().StopSubs();
        //GetComponentInParent<Interactive>().TurnOnRenderer();

        PauseButton.SetActive(false);
        PlayButton.SetActive(false);
        StopButton.SetActive(false);

        //need to reload scene for play testing
        Controller.s.ReloadSceneAndMoveRooms(roomNumber);
    }

    public void StopWithoutRenderer() {
        GetComponentInParent<VideoPlayer>().Stop();
        GetComponentInParent<Interactive>().StopSubs();
        //GetComponentInParent<Interactive>().TurnOnRenderer();


        PauseButton.SetActive(false);
        PlayButton.SetActive(false);
        StopButton.SetActive(false);
    }

    public void ToggleCaptions(bool show) {
        Controller.s.showCaptions = show;

        if (show) {
            if (Controller.s.activeControls) {
                Controller.s.activeControls.GetComponentInParent<Interactive>().StartSubs();
            }
        } else {
            if (Controller.s.captionText) {
                Controller.s.captionText.text = "";
            }
        }
    }
}
