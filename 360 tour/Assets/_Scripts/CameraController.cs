﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CameraController : MonoBehaviour
{
    //public static CameraController s; // maybe?
    public Vector2 rotation = new Vector2(0, 0);
    public float speed = 3;

    public Transform nextPosition;

    public Transform[] rooms;
    public GameObject fastTravel;
    public GameObject joystick;

    void Update() {
        rotation.y += Input.GetAxis("Horizontal");
        rotation.x += Input.GetAxis("Vertical");
        transform.eulerAngles = (Vector2)rotation * speed;
    }

    internal void PlayAnim(Transform destination) {
        nextPosition = destination;
        GetComponentInChildren<Animation>().Play();
    }

    public GameObject lgTv, leftTv, rightTv;
    public int lgTvRoom = 9, twinTvRoom = 10;

    public void teleportTo(int room) {
        if (!Controller.s.firstLoad) {
            fastTravel.SetActive(true);
        }
        nextPosition = rooms[room];
        //print(room + " " + nextPosition.name);

        foreach (var item in nextPosition.GetComponentsInChildren<VideoPlayer>()) {
            item.enabled = true;
        }

        //if (room == lgTvRoom) {
        //    lgTv.SetActive(true);
        //} else if (room == twinTvRoom) {
        //    leftTv.SetActive(true);
        //    rightTv.SetActive(true);
        //}

        Teleport();
    }

    public void Teleport() {
        if (!nextPosition) {
            return;
        }

        transform.position = nextPosition.position;
        rotation = Vector3.zero;
        //transform.eulerAngles = Quaternion.LookRotation(nextPosition.GetComponent<TeleportPosition>().lookTarget.position - transform.position).eulerAngles;
        nextPosition = null;
    }
}
