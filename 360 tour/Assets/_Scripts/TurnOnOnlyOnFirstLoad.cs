﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnOnlyOnFirstLoad : MonoBehaviour {

    public List<GameObject> toenable;
	// Use this for initialization
	void Start () {
        if (Controller.s) {
            if (!Controller.s.firstLoad) {
                gameObject.SetActive(false);
                foreach (var item in toenable) {
                    item.SetActive(true);
                }
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
