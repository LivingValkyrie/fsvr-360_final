﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartTour : MonoBehaviour {
	public void PlayAgain () {
        Controller.s.firstLoad = true;
        Controller.s.ReloadSceneAndMoveRooms(0);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
