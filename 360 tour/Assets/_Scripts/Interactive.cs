﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Interactive : MonoBehaviour
{

    //public Color[] colors;
    //int i = 0;

    string url;
    public string filename;
    public bool useable = true;
    public float delay = 0.1f;

    public MeshRenderer renderTarget;
    public RenderTexture renderTexture;
    public Texture staticTexture;
    public bool isPrepped = false;

    public List<GameObject> objectsToDisable;
    public int roomNumber;

    public List<ClosedCaption> captions;
    public GameObject videoControls;

    void Start() {
        if (ignoreIos) {
            return;
        }
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android) {
            useURL = false;
        }

        //    if (GetComponent<VideoPlayer>()) {
        //        GetComponent<VideoPlayer>().Prepare();
        //    }
    }

    public bool ignoreIos = false;
    bool useURL = true;

    [Button]
    public void PrepVideo()
    {
        if (GetComponent<VideoPlayer>())
        {
            url = GetComponent<VideoPlayer>().url;
            if (string.IsNullOrEmpty(url))
            {
                url = System.IO.Path.Combine(Application.streamingAssetsPath, filename);
            }


            if (useURL)
            {

                GetComponent<VideoPlayer>().url = url;
            }

            //GetComponent<VideoPlayer>().renderMode = VideoRenderMode.RenderTexture;
            //GetComponent<VideoPlayer>().targetTexture = renderTexture;

            if (GetComponent<MeshRenderer>()) {

                renderTarget = transform.parent.GetComponent<MeshRenderer>();
            }

            isPrepped = true;

            //GetComponent<VideoPlayer>().Prepare();
            StartCoroutine("PrepareVideo");
        }

        GetComponent<VideoPlayer>().loopPointReached += ReplaceTexture;
    }

    public Material test;
    void ReplaceTexture(VideoPlayer vp) {
        //print("replace texture called");
        //vp.targetTexture.Release();
        vp.enabled = false;
        //test.SetTexture("_MainTexture", staticTexture);
        //renderTarget.material = test;
    }

    IEnumerator PrepareVideo() {
        var v = GetComponent<VideoPlayer>();
        WaitForSeconds delay = new WaitForSeconds(0.25f);

        while (!v.isPrepared) {
            //print("video not prepared");
            yield return delay;
            break;
        }
    }

    //public int roomNumber;

    internal virtual void OnMouseDown() {
        if (useable) {
            //if (GetComponent<VideoPlayer>()) {
            //    if (!GetComponent<VideoPlayer>().enabled) {
            //        GetComponent<VideoPlayer>().enabled = true;
            //    }
            //}
            //Interact();
            //videoControls.SetActive(true);
            useable = false;

            Controller.s.ReloadThenPlayVideo(roomNumber);
        }
    }


    public virtual void Interact() {
        useable = false;


        //i++;
        //if (i >= colors.Length) {
        //    i = 0;
        //}
        //GetComponent<Renderer>().material.color = colors[i];
        //Controller.s.LoadTour(roomNumber);
        if (!isPrepped) {
            PrepVideo();
        }

        foreach (var g in objectsToDisable) {
            g.SetActive(false);
        }

        //GetComponent<VideoPlayer>().SetTargetAudioSource(0, GetComponent<AudioSource>());

        //GetComponent<VideoPlayer>().EnableAudioTrack(0, true);
        //GetComponent<VideoPlayer>().SetTargetAudioSource(0, GetComponent<AudioSource>());
        //if (renderTexture && renderTarget) {
        //    renderTarget.material.SetTexture("_MainTex", renderTexture);
        //}

        if (GetComponent<VideoPlayer>().isPlaying) {
            GetComponent<VideoPlayer>().Stop();
        } else {
            GetComponent<VideoPlayer>().Play();
            //print("playing");
            //GetComponent<AudioSource>().Play();
        }

        if (videoControls) {
            videoControls.SendMessage("EnableVidControls");
            Controller.s.activeControls = videoControls.GetComponent<VideoControls>();
        }

        if (GetComponent<SpriteRenderer>()) {
            useable = false;
            GetComponent<SpriteRenderer>().enabled = false;
            //Invoke("TurnOnRenderer", delay);
        }

        StartCoroutine("UpdateSubs");
    }

    public void StopSubs() {
        StopCoroutine("UpdateSubs");
        if (Controller.s.captionText) {
            Controller.s.captionText.text = "";
        }

    }

    public void StartSubs() {
        StopCoroutine("UpdateSubs");
        //if (Controller.s.captionText) {
        //    Controller.s.captionText.text = "";
        //}
        StartCoroutine("UpdateSubs");
    }

    IEnumerator UpdateSubs() {
        if (!GetComponent<VideoPlayer>()) {
            yield return new WaitForEndOfFrame();
        }

        //GetComponent<VideoPlayer>().time

        if (!Controller.s.captionText) {
            if (GameObject.Find("CaptionsText")) {
                Controller.s.captionText = GameObject.Find("CaptionsText").GetComponent<Text>();
            }
        }

        while (GetComponent<VideoPlayer>().isPlaying && Controller.s.showCaptions) {
            foreach (var sub in captions) {
                //print(GetComponent<VideoPlayer>().time);
                if (GetComponent<VideoPlayer>().time > sub.timestamp) {
                    Controller.s.captionText.text = sub.caption;
                } else {
                    break;
                }
            }
            yield return new WaitForFixedUpdate();
        }

        //yield return OldSubs();

        //Controller.s.captionText.text = "";
    }

    IEnumerator OldSubs() {
        float startTime = Time.timeSinceLevelLoad;
        for (int i = 0; i < captions.Count; i++) {
            if (!Controller.s.captionText) {
                Controller.s.captionText = GameObject.Find("CaptionsText").GetComponent<Text>();
            }
            yield return new WaitForSeconds(startTime + captions[i].timestamp - Time.timeSinceLevelLoad);
            Controller.s.captionText.text = captions[i].caption;
        }
    }

    public void TurnOnRenderer() {
        if (Controller.s.captionText) {
            Controller.s.captionText.text = "";
        }

        Controller.s.activeControls = null;

        if (GetComponent<SpriteRenderer>()) {
            foreach (var g in objectsToDisable) {
                g.SetActive(true);
                if (GetComponent<VideoPlayer>()) {
                    g.GetComponent<VideoPlayer>().Play();
                }

            }

            GetComponent<SpriteRenderer>().enabled = true;
            if (staticTexture) {
                //print("replacing texture when renderer turns on");
                renderTarget.material.SetTexture("_MainTex", staticTexture);
            }
            useable = true;
        }
    }


    private void OnDisable() {
        if (GetComponent<VideoPlayer>()) {

            GetComponent<VideoPlayer>().loopPointReached -= TurnOffControls;
        }
    }

    private void OnEnable() {
        if (GetComponent<VideoPlayer>()) {

            GetComponent<VideoPlayer>().loopPointReached += TurnOffControls;
            
        }

    }

    void TurnOffControls(VideoPlayer vp) {
        TurnOffVidControls();
    }

    public void TurnOffVidControls(bool teleporting = false) {
        if (videoControls) {
            if (teleporting) {
                videoControls.SendMessage("StopWithoutRenderer");
            } else {
                videoControls.SendMessage("StopVideo");
            }
        }
    }
}
