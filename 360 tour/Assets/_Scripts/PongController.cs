﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PongController : MonoBehaviour {

    //bool isPlaying = false;
    int countDown = 3, player4gScore, player5gScore;
    public Text countText, player4gScoreText, player5gScoreText;
    public GameObject replayPanel, instructions;


    public Image ball, paddle4G, paddle5G;
    public int dir = 1;

    // Update is called once per frame
    //void Update() {
    //    if (Input.GetKeyDown(KeyCode.Space)) {
    //        if (ball.GetComponent<PongBall>().canMove) {
    //            return;
    //        }

    //        StartTurn();
    //    }

    //}

    public GameObject line;

    public void Reset() {
        player4gScore = player5gScore = 0;
        player4gScoreText.text = player4gScore.ToString();
        player5gScoreText.text = player5gScore.ToString();
        line.SetActive(true);
        StartTurn();
    }

    public void ScorePoint(bool player4G) {
        if (player4G) {
            player4gScore++;
            player4gScoreText.text = player4gScore.ToString();
        } else {
            player5gScore++;
            player5gScoreText.text = player5gScore.ToString();
        }

        if (player5gScore >= 3) {
            countText.text = "5G Wins!";
            countText.transform.parent.gameObject.SetActive(true);
            replayPanel.SetActive(true);
            return;
        }

        if (player4gScore >= 3) {
            countText.text = "4G Wins!";
            countText.transform.parent.gameObject.SetActive(true);
            replayPanel.SetActive(true);
            return;
        }

        StartTurn();

    }

    public void StartTurn() {
        replayPanel.SetActive(false);
        StartCoroutine("Begin");
    }

    IEnumerator Begin() {
        countText.transform.parent.gameObject.SetActive(true);
        for (int i = countDown; i > 0; i--) {
            countText.text = i.ToString();
            yield return new WaitForSeconds(1);
        }

        countText.text = "GO!";
        instructions.SetActive(false);
        yield return new WaitForSeconds(0.5f);

        countText.transform.parent.gameObject.SetActive(false);

        ball.GetComponent<PongBall>().StartMoving(dir);
        dir = -dir;
    }
}

/*
 
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     */
