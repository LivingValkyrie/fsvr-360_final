﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PongPaddle : MonoBehaviour, IDragHandler , IBeginDragHandler{

    public bool hasDelay = false;
    public float ping = 0.1f;
    float delay = 0;
    public float maxY = 1f;
    public bool playerControlled = false;
    public PongPaddle opponent;
    public Transform ball;

    public Transform maxPoint, minPoint;

    // Use this for initialization
    void Start() {
        if (hasDelay) {
            delay = ping;
        }
    }

    // Update is called once per frame
    void Update() {
        if (playerControlled) {
            return;
        }

        var tPos = transform.position;

        tPos.y = Mathf.Clamp(ball.position.y, minPoint.position.y, maxPoint.position.y);

        StartCoroutine("MoveWithDelay", tPos);
    }

    public void OnDrag(PointerEventData eventData) {
        //Vector3 v3;
        //if (RectTransformUtility.ScreenPointToWorldPointInRectangle(transform.root.GetComponent<Canvas>().transform as RectTransform, eventData.position, eventData.pressEventCamera, out v3)) {
            var tPos = transform.position;

            tPos.y = Mathf.Clamp(eventData.position.y, minPoint.position.y, maxPoint.position.y);

            StartCoroutine("MoveWithDelay", tPos);
        //}
    }

    IEnumerator MoveWithDelay(Vector3 tPos) {
        yield return new WaitForSeconds(delay);
        //GetComponent<Rigidbody2D>().MovePosition(tPos);
        transform.position = tPos;
    }

    //public void OnMouseDown() {

    //}

    public void OnBeginDrag(PointerEventData eventData)
    {
        playerControlled = true;
        opponent.playerControlled = false;
    }

    //public void OnDrag(PointerEventData eventData)
    //{
    //    Vector3 v3 = transform.position;
    //    v3.y = Input.mousePosition.y;

    //    transform.position = v3;
    //}
}
