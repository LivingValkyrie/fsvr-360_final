﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class BrakeTest : MonoBehaviour {
    public GameObject brakePrompt;
    public float brakeDelay = 0f;
    public bool brakePressed = false;

    public GameObject autoResult;
    public GameObject reactResult;
    public Text reactTime;
    public float resultDelay = 0f;

    public GameObject finishButton;
    public float finishDelay = 0f;

    public GameObject startButton;
    public Interactive videoPlayer;
    public GameObject still;

    public VideoPlayer actualPlayer;

    public GameObject prepareToBrakeText;
    public void StartGame() {
        StartCoroutine("BeginTest");
        startButton.SetActive(false);
        still.SetActive(false);
        videoPlayer.Interact();
    }

    public void Start() {
        //startButton.SetActive(true);
        still.SetActive(true);
        brakePrompt.SetActive(false);
        autoResult.SetActive(false);
        reactResult.SetActive(false);
        finishButton.SetActive(false);
        prepareToBrakeText.SetActive(false);
    }

    public void Brake() {
        brakeTime = Time.timeSinceLevelLoad;
        difference = brakeTime - startTime;

        brakePressed = true;
        reactTime.text = string.Format("{0:N3} Seconds!", difference);

        brakePrompt.SetActive(false);
    }

    float startTime, brakeTime, difference;

    IEnumerator BeginTest() {
        do {
            //print("not prepped");
            yield return new WaitForEndOfFrame();
        } while (!actualPlayer.isPrepared); // was (!actualPlayer.isPlaying) James fixed it woohoo
        //print("prepped");

        yield return new WaitForSecondsRealtime(brakeDelay-3);
        prepareToBrakeText.SetActive(true);

        for (int i = 0; i < 3; i++) {
            prepareToBrakeText.GetComponentInChildren<Text>().text += ".";
            yield return new WaitForSeconds(1);
        }

        prepareToBrakeText.SetActive(false);
        brakePrompt.SetActive(true);
        startTime = Time.timeSinceLevelLoad;

        yield return new WaitForSeconds(resultDelay);
        //brakePrompt.SetActive(false);

        if (brakePressed == true) {
            reactResult.SetActive(true);
        } else {
            autoResult.SetActive(true);
            brakePrompt.SetActive(false);

        }

        yield return new WaitForSeconds(finishDelay);
        finishButton.SetActive(true);
    }

    public void HitBrake() {
        Brake();
    }
}