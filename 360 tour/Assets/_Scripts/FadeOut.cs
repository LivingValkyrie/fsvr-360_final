﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : MonoBehaviour {

    public float delay = 0f;
    private Animator anim;
    
    // Use this for initialization
    IEnumerator Start ()
    {
        anim = GetComponent<Animator>();
        anim.enabled = false;
        yield return new WaitForSecondsRealtime(delay); // wait for countdown to finish
        anim.enabled = true;
    }

}
