﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PongBall : MonoBehaviour {
    public PongController controller;
    public Vector3 velocity;
    public bool canMove = false;
    public float speed = 1;
    public float yRangeForStart = 0.5f;
    Vector3 startingPos;

    void Start() {
        startingPos = transform.position;
    }

    // Use this for initialization
    public void StartMoving(int dir) {
        velocity = new Vector3(dir, Random.Range(-yRangeForStart, yRangeForStart));
        canMove = true;
        GetComponent<Image>().enabled = true;
        GetComponent<Renderer>().enabled = true;

    }

    // Update is called once per frame
    void LateUpdate() {
        if (canMove) {
            transform.position += velocity * Time.deltaTime * speed;
        }
    }

    void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "Paddle") {
            velocity.x *= -1;
        } else if (other.gameObject.tag == "Wall") {
            velocity.y *= -1;
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Goal4G") {
            controller.ScorePoint(true);

            //print("sdfdsfdsf");
            //grant points to 5g
            Reset();
        } else if (other.gameObject.tag == "Goal5G") {
            //print("sdfdsfdsf");
            controller.ScorePoint(false);

            //grant points to 4g
            Reset();
        }
    }

    void Reset() {
        transform.position = startingPos;
        velocity = Vector3.zero;
        canMove = false;
        GetComponent<Image>().enabled = false;
        GetComponent<Renderer>().enabled = false;

    }
}
