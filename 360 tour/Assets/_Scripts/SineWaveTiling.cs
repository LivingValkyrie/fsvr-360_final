﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineWaveTiling : MonoBehaviour {

    public float ScaleFactor = 1;
    public Renderer targetImage;

    public void ChangeTiling(float value) {
        targetImage.material.mainTextureScale = new Vector2(value * ScaleFactor, 1);
    }
}
