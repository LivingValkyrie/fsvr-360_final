﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasCloseButton : MonoBehaviour {

    public GameObject joystick, toDisable;

    // Update is called once per frame
    public void Interact() {
        joystick.SetActive(true);
        toDisable.SetActive(false);
    }
}
