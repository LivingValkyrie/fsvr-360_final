﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MovementArrow : MonoBehaviour
{

    public Transform destination;
    public Transform pivot;
    [Tooltip("This gameobject will be enabled whenever this arrow is used. using it to enable back button for first room once the last room has been visited.")]
    public GameObject toEnable;
    public int destinationRoomNumber;

    public Interactive toDisableHolder;

    //[Header("new fucntionality")]
    //public int nextRoom;

    private void OnMouseDown() {
        NewInteract();
    }

    //use this method in the teleport button, 
    //assign the movement arrow that GOES TO the sphere you are teleporting to
    void NewInteract() {
        //GetComponent<Renderer>().enabled = false;
        foreach (var item in transform.root.GetComponentsInChildren<VideoPlayer>()) {
            item.enabled = false;
        }

        pivot.GetComponent<CameraController>().teleportTo(destinationRoomNumber);

        if (transform.parent.GetComponentInChildren<VideoPlayer>()) {
            transform.parent.GetComponentInChildren<VideoPlayer>().Stop();
        }

        foreach (var item in transform.parent.GetComponentsInChildren<Interactive>()) {
            item.StopAllCoroutines();
            item.TurnOffVidControls();
        }

        if (Controller.s.captionText) {
            Controller.s.captionText.text = "";
        }

        if (toDisableHolder) {
            foreach (var g in toDisableHolder.objectsToDisable) {
                g.SetActive(false);
            }
        }

        Controller.s.ReloadSceneAndMoveRooms(destinationRoomNumber);

        Render();
    }

    //use this method in the teleport button, 
    //assign the movement arrow that GOES TO the sphere you are teleporting to
    public void TeleportToDestination(int i) {
        foreach (var arrow in FindObjectsOfType<MovementArrow>()) {
            if (arrow.transform.parent.GetComponent<VideoPlayer>()) {
                //GetComponent<Renderer>().enabled = false;
                arrow.transform.parent.GetComponentInChildren<VideoPlayer>().Stop();
            }
            foreach (var item in arrow.transform.parent.GetComponentsInChildren<Interactive>()) {
                //print(item.name);

                item.StopAllCoroutines();
                item.TurnOffVidControls(true);
            }
        }

        if (Controller.s.captionText) {
            Controller.s.captionText.text = "";
        }

        //Controller.s.UpdateRoomName(i);

        Render();

        //pivot.GetComponent<CameraController>().teleportTo(i);
        Controller.s.ReloadSceneAndMoveRooms(i);

    }

    public void Interact() {
        GetComponent<Renderer>().enabled = false;
        //Debug.Log("disabled renderer");
        pivot.GetComponent<CameraController>().PlayAnim(destination);
        //CameraController.s.PlayAnim(destination); // maybe?
        //Debug.Log("played animation");

        //foreach (var v in FindObjectsOfType<VideoPlayer>()) {
        //    if (v.gameObject.tag != "TV" && v.GetComponent<VideoPlayer>().isPlaying == true) { //check if playing too?
        //        v.Stop();
        //        Debug.Log("stopped " + v.gameObject.name);
        //    }
        //}

        Invoke("Render", 1.5f);
        //pivot.position = destination.position;
        //Debug.Log("reenabling renderer in 1.5f");
        foreach (var r in FindObjectsOfType<Interactive>()) {
            r.TurnOnRenderer();
            //Debug.Log("Reenabling render for " + r.gameObject.name);
        }

        if (toEnable) {
            toEnable.SetActive(true);
            //Debug.Log("setting ToEnable to true");
        }


        Controller.s.LoadTour(destinationRoomNumber);
        //Debug.Log("loading tour with room" + destinationRoomNumber);
        //CameraController.s.teleportTo(destinationRoomNumber); // maybe?
    }

    private void Render() {
        GetComponent<Renderer>().enabled = true;
        //Debug.Log("Reenabling renderer");
    }
}
