﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroOutro : MonoBehaviour {
    public List<GameObject> Logos;
    public List<GameObject> Pings;
    public float pingDelay = 0f;

    public GameObject title;
    public GameObject titlePing;
    public float titleDelay = 0f;

    public Animator anim;
    public float blackDelay = 0f;

    public GameObject controls,joystick;

    void Start() {
        if (!Controller.s.firstLoad) {
            gameObject.SetActive(false);
            controls.SetActive(false);
            joystick.SetActive(true);
            return;
        }

        for (var i = 0; i < Logos.Count; i++) {
            if (Logos[i].activeInHierarchy == true) {
                Logos[i].SetActive(false); // disable all logos
            }

            if (Pings[i].activeInHierarchy == true) {
                Pings[i].SetActive(false); // disable all pings
            }
        }

        title.SetActive(false);
        titlePing.SetActive(false);
        anim.enabled = false;// disable animator
        controls.SetActive(false);

        Logos[0].SetActive(true); // enable first logo
        Pings[0].SetActive(true); // enable first ping
        StartCoroutine(LateCall());
    }

    // enable one at a time
    IEnumerator LateCall() {
        for (var i = 0; i < Logos.Count - 1; i++) {
            yield return new WaitForSeconds(pingDelay); // wait for animation to finish
            Logos[i].SetActive(false); // disable current logo
            Pings[i].SetActive(false); // disable current ping
            Logos[i + 1].SetActive(true); // enable next logo
            Pings[i + 1].SetActive(true); // enable next ping
        }
        yield return new WaitForSeconds(pingDelay);
        Pings[Logos.Count - 1].SetActive(false);
        Logos[Logos.Count - 1].SetActive(false);

        title.SetActive(true);
        titlePing.SetActive(true);
        yield return new WaitForSeconds(titleDelay); // wait for animation to finish
        title.SetActive(false);
        titlePing.SetActive(false);
        controls.SetActive(true);
        anim.enabled = true; // fade out
        yield return new WaitForSeconds(blackDelay); // wait for animation to finish

    }
}