﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStarter : MonoBehaviour {

    public bool loadPong = true;

    // Update is called once per frame
    public void OnMouseDown() {
        if (loadPong) {
            Controller.s.LoadPong();
        } else {
            Controller.s.LoadBrakeTest();
        }
    }
}
