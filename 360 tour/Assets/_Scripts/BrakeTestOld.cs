﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class BrakeTestOld : MonoBehaviour {
    string url;
    public string filename;
    public GameObject tv;
    public VideoPlayer videoPlayer;
    public AudioSource audioSource;
    public float vidLength;

    private void OnMouseDown()
    {
        tv.SetActive(true);
        StartCoroutine(PlayVideo());
    }

    IEnumerator PlayVideo()
    {
        if (string.IsNullOrEmpty(url))
        {
            url = System.IO.Path.Combine(Application.streamingAssetsPath, filename);
        }

        videoPlayer.url = url;
        videoPlayer.Prepare();
        WaitForSeconds delay = new WaitForSeconds(1);

        while (!videoPlayer.isPrepared)
        {
            yield return delay;
            break;
        }

        videoPlayer.Play();
        audioSource.Play();

        WaitForSeconds playDelay = new WaitForSeconds(vidLength);
        yield return playDelay;

        videoPlayer.Stop();
        audioSource.Stop();

        tv.SetActive(false);
    }
}
