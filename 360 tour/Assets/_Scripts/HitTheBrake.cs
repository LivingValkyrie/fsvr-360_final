﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitTheBrake : MonoBehaviour
{
    public GameObject brakeButton;
    public Color solidColor;
    public Color flashColor;
    public float speed = 0.1f;

	// Use this for initialization
	void Start ()
    {
        brakeButton.GetComponent<Image>().color = solidColor;
        InvokeRepeating("UpdateSprite", speed, speed);
	}
	
	// Update is called once per frame
	void UpdateSprite ()
    {
        if (brakeButton.GetComponent<Image>().color == solidColor)
        {
            brakeButton.GetComponent<Image>().color = flashColor;
        }
        else
        {
            brakeButton.GetComponent<Image>().color = solidColor;
        }
	}
}
