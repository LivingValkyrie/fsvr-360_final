﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class StaticVideo : MonoBehaviour
{
    string url;
    public string filename;
    // Use this for initialization
    void OnEnable() {
        if (GetComponent<VideoPlayer>()) {
            url = GetComponent<VideoPlayer>().url;
            if (string.IsNullOrEmpty(url)) {
                url = System.IO.Path.Combine(Application.streamingAssetsPath, filename);
            }
            GetComponent<VideoPlayer>().url = url;
            GetComponent<VideoPlayer>().Play();
        }
    }

}
