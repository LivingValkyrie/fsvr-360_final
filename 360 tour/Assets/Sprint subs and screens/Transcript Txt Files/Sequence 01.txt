So as our customers demand more and more from our network
represented in our city here,
you can see that they're accessing from their cars along the street.
And because they want such a rich data experience
we need to bring the network closer to them.
Starting with our macro tower
we might look at a city model like this and put in mini macros.
We might also put antenna atop polls like this.
Perhaps we have an award winning Magic Box in the window of the bistro
or the shop down there or one of the residents down here or maybe even
we have a strand mount antenna that is hung from utility line.
The cool part about this is the backhaul from antennas
like this backhaul to the tower
so we don't have to go through a lot of permitting
or get the city's permission in order to install these.
The whole point is network densification.
It's bringing our network, our 5G network,
closer to the people who are using it.