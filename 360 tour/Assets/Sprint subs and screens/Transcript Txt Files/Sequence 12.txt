Security through video is certainly something that
concerns us all and with 5G that great throughput to
handle video streams, but also control through low latency
is going to be critical to how we have security through video
at home and at work and the analytics that come with it.
So looking at various analytics like heat mapping,
like people counting, monitoring a parking garage,
certainly the quality will be better.
The clarity of image will be better.
The ability to zoom and pan in will be enhanced by
lower latency along with the ability to scale up quickly
with IOT devices like cameras through the power of 5G.